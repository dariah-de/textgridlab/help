#!/bin/sh
keep=no
force=no
file=

help() {
    cat <<EOF
$0 -- TextGridLab Help Preparation Script

Synopsis:
    $0 [-k] zipfile

EOF
    exit 1
}

while [ $# -gt 0 ]
do
    case "$1" in
	-k)		keep=yes;;
	-f)	        force=yes;;
	-h|--help)	help;;
	*)		file="$1";;
    esac
    shift
done

if [ "$file" = "" ]
then
    echo "ERROR: No export file specified."
    echo
    help
fi


### Setup
src=`pwd`
target="`pwd`/help2.0"
bold=`tput bold` 
boldoff=`tput sgr0`

### Are we ready to rumble?
if [ -z "`git status --porcelain`" ]
then
    echo Working on a clean working copy -- preparing a commit
elif [ "$force" = "yes" ]
then
    git status
    echo "${bold}Forcibly running on a non-clean working copy${boldoff}"
    echo
else
    git status
    echo "${bold}Refusing to work on a non-clean working copy${boldoff}"
    echo
    echo "Commit or stage your changes, then run this command again."
    echo '(-f for force, but this will cause trouble.)'
    exit 128
fi

tmp=`mktemp -d`

echo "${bold}0. Unzipping $file ...${boldoff} (temporary directory $tmp)"
unzip -q -d "$tmp" "$file"


cd "$tmp"

echo
echo "${bold}1. HTML tidying and XHTML conversion ...$boldoff" >&2
mkdir tidied
for f in TextGrid/*.html; do
    # new name w/o '+'
    out="tidied/`basename \"$f\" | sed -e 's/+/ /g'`"
    echo ">>> $f > $bold$out$boldoff ..." >&2
    tidy -asxhtml --input-encoding utf8 --output-encoding utf8 --quote-nbsp no --indent yes --wrap 1000 --numeric-entities yes --doctype omit -q --tidy-mark no --new-blocklevel-tags section,nav -o "$out" "$f"
    if [ $? -gt 1 ]
    then
	echo "${bold}Errors tidying $f.$boldoff Exiting, temporary directory left at $tmp"
	exit 2
    fi
done

echo
echo "${bold}2. HTML postprocessing and attachment collection ...$boldoff" >&2
for f in  tidied/*.html; do
    echo "   $f ..." >&2
    xsltproc  --novalid --nonet -o "prepared/`basename \"$f\"`" "$src/rmattachmentsection.xsl" "$f" 2>&1
done > attachments.lst

echo -n "${bold}Summarizing ... ${boldoff}" 2>&1
( cd TextGrid && find attachments -type f | fgrep -v -f ../attachments.lst ) > unused-attachments.lst
echo "`wc -l attachments.lst` attachments used, `wc -l unused-attachments.lst` unused." >&2

echo "${bold}3. Removing unused attachments ...${boldoff}" 2>&1
( cd TextGrid && xargs -d '\n' -a ../unused-attachments.lst rm ) 

echo "${bold}4. Optimizing remaining PNG files ...${boldoff}" 2>&1

(
    cd TextGrid
    prev=`du -hcs attachments`
    grep '\.png$' ../attachments.lst | while read png
    do
	echo "  Crushing $png ..."
	cp "$png" "$png".orig
	pngcrush -q "$png".orig "$png"
	rm "$png".orig
    done
    echo "$prev	before" >&2
    echo "`du -hcs attachments` after" >&2
)


#### Now prepare SVN

echo "${bold}5. Preparing working copy.${boldoff}"

echo "5.1 Copying up-to-date files in place"
rm TextGrid/*.html
mv prepared/*.html TextGrid

rm -rf -- "$src/help2.0/"*
cp -rp "$tmp/TextGrid/"* "$src/help2.0"

echo "5.2 preparing TOC"
( cd "$src" && xsltproc --novalid -o toc.xml confluence2toc.xsl help2.0/index.html )

echo "5.3 adding changes to scm"
( cd "$src" && git add -A -- toc.xml "$src/help2.0" )

if [ "$keep" = "no" ]
then
    echo "${bold}Removing temporary directory.${boldoff} Use -k to keep it."
    cd "$src"
    rm -rf "$tmp"
else
    echo "Temporary files remain in ${bold}$tmp${boldoff}".
fi

echo
echo "Your working tree has been prepared using the changes from $file".
echo "${bold}You can now commit the changes using \`git commit'${boldoff}"
echo "Use \`git status' or \`git diff --cached' to review the changes,"
echo "undo all changes by \`git reset --hard'"
git diff --cached --shortstat
