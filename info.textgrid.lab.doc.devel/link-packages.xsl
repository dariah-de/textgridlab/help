<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xh="http://www.w3.org/1999/xhtml">

	<!-- -->
	<xsl:template match="*[@class='package']">
		<xsl:copy>
			<xsl:copy-of select="@*" />
			<a href="../javadoc/{translate(., '.', '/')}/package-summary.html">
				<xsl:apply-templates />
			</a>
		</xsl:copy>
	</xsl:template>


	<!-- standard copy template -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>