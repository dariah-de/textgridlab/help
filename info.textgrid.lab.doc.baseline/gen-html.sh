#!/bin/sh

if [ -d tex ]
then
    ( cd tex; svn update )
else
    svn checkout https://develop.sub.uni-goettingen.de/repos/textgrid/trunk/textformate/kerncodierung/dokumentation/tex
fi
if [ -d tex_en ]
then
    ( cd tex_en; svn update )
else
    svn checkout https://develop.sub.uni-goettingen.de/repos/textgrid/trunk/textformate/kerncodierung/dokumentation/tex_en
fi

for d in tex/*
do
    destdir=html/`basename $d`
    if [ -d tex_en/`basename $d` ]
    then
	d=tex_en/`basename $d`
    fi

    for f in $d/*.tex
    do
	mkdir -p $destdir
	latex2html -init_file latex2html-init.pl -dir $destdir \
    	   -split +1 -local_icons -address 'TextGrid' -info '' $f
    done
done

tidy -m -asxhtml --input-encoding latin1 --output-encoding utf-8 html/*/*.html

