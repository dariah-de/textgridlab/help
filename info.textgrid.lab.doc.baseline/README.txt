This plugin includes the baseline encoding's documentation with TextGridLab's
help system.


			    FILES AND DIRECTORIES


build.properties
plugin.xml
META-INF/MANIFEST.MF

    Eclipse's Plugin stuff.

gen-html.sh

    Shell script to generate html docs, see below

latex2html-init.pl

    Configuration file for latex2html

html

    The generated (!) baseline encoding documentation files. As it requires
    some special setup to generate these files, they've been checked in to svn
    as well.
    
toc.xml

    The help table of contents

Not in SVN:

tex
    The checked-out version of the TeX files. You find them at
    https://develop.sub.uni-goettingen.de/repos/textgrid/trunk/textformate/kerncodierung/dokumentation/tex



			GENERATION SCRIPT gen-html.sh

The gen-html.sh script currently checks out the tex files from svn, calls
latex2html with sensible arguments on the files and then calls tidy to convert
the stuff from HTML/latin1 to XHTML/UTF8. It requires, well, latex2html and
HTML tidy to be on the path.
